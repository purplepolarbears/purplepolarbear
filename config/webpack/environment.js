const { environment } = require('@rails/webpacker');
const webpack = require('webpack');

environment.plugins.prepend(
  'Provide',
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    jquery: 'jquery',
    'window.Tether': 'tether',
    Popper: ['popper.js', 'default'],
  })
);

environment.loaders.append('images',
  {
    test: /\.(png|jpg)$/,
    loader: 'file-loader',
    options: {
      name: '[name][md5:hash].[ext]', // Name of bundled asset
      outputPath: 'images/', // Output location for assets. Final: `app/assets/webpack/webpack-assets/`
      publicPath: '/images/' // Endpoint asset can be found at on Rails server
    }
  }
)

module.exports = environment
